# Very simple script to ouput lines for fault injection for our system

n_lines=$(eval ovs-ofctl dump-flows s2 | grep icmp | wc -l)
echo $n_lines

for i in `seq 1 $n_lines`
do
	base_cmd="ovs-ofctl dump-flows s2 | grep icmp | sed -n '${i}p' | \
	          sed 's/ actions/,actions/' | sed 's/ //' | sed 's/.*icmp,/icmp,/'"
    line=$(eval $base_cmd | sed 's/output:4//')
    echo "sudo ovs-ofctl mod-flows s2 $line"
done