/*
 * Switch.hh
 *
 *  Created on: Jul 22, 2016
 *      Author: miranda
 */

#ifndef SWITCH_HH_
#define SWITCH_HH_

#include <list>
#include <utility>
#include <stdint.h>

class Switch {
public:
	const std::list<const char*>& getTable(uint64_t version) const;
	void addFlow(const char* flow);
	void modFlows(const char* flow);
	void delFlows(const char* match);

private:
	std::list<std::pair<uint64_t, std::list<const char*>>> tables;
};

#endif /* SWITCH_HH_ */
