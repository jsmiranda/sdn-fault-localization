/*
 * Copyright 2014, Stanford University. This file is licensed under Apache 2.0,
 * as described in included LICENSE.txt.
 *
 * Author: nikhilh@cs.stanford.com (Nikhil Handigol)
 *         jvimal@stanford.edu (Vimal Jeyakumar)
 *         brandonh@cs.stanford.edu (Brandon Heller)
 */

#include <cstdio>
#include <vector>
#include <iostream>
#include <sys/time.h>
#include <pcap.h>
#include <pthread.h>

#include "netsight.hh"
#include "EquivalenceClass.h"
#include <unordered_map>

#define SNAP_LEN 1514
#define POSTCARD_FILTER ""
#define DEBUG_VLAN 0x02
static int SKIP_ETHERNET = 0;

using namespace std;

unordered_map<EquivalenceClass, multimap<string, string>> pathsFromVF;
void rewindTwoSwitches(const char *&path, const char *pinit);
bool compare_paths(string realPath, string expectedPath);
void perform_check(PostcardList& packet_history);
int ph_period = PACKET_HISTORY_PERIOD;

NetSight::NetSight():
    context(1)
{
    /* Database handler initialization */
    topo_db.set_db("ndb");
    topo_db.set_coll("topo");

    ft_db.set_db("ndb");
    ft_db.set_coll("flow-tables");

    psid_db.set_db("ndb");
    psid_db.set_coll("psid-to-dpid");

    /* Initialize mutexes and condition variables */
    pthread_mutex_init(&stage_lock, NULL);

    /* set of signals that the signal handler thread should handle*/
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGINT);
}

void
NetSight::start()
{
    /* block out these signals 
     * any newly created threads will inherit this signal mask
     * */
    pthread_sigmask(SIG_BLOCK, &sigset, NULL);

    /* Start postcard_worker thread */
    pthread_create(&postcard_t, NULL, &NetSight::postcard_worker, NULL);

    /* Start history worker thread */
    pthread_create(&history_t, NULL, &NetSight::history_worker, NULL);

    /* Unblock the signals */
    pthread_sigmask(SIG_UNBLOCK, &sigset, NULL);

    /* create a signal handler */
    signal(SIGINT, sig_handler);

    //interact();
    serve();
}

/*
void
NetSight::interact()
{
    cout << "Enter a PHF to add or \"exit\" to exit.";
    while(true)
    {
        cout << ">>";
        string input;
        cin.clear();
        cin.sync();
        if (!getline(cin, input)) {
            ERR("Error: Reading user input\n");
        }
        DBG("Got input\n");
        if(input == "exit") {
            cleanup();
            exit(EXIT_SUCCESS);
        }
        else if(input == "")
            continue;
        regexes.push_back(input);
        filters.push_back(PacketHistoryFilter(input.c_str()));
    }
}
*/

void
NetSight::serve()
{
    zmq::socket_t rep_sock(context, ZMQ_ROUTER);
    int linger = 0;
    stringstream rep_ss;
    rep_ss << "tcp://*:" << NETSIGHT_CONTROL_PORT;
    rep_sock.bind(rep_ss.str().c_str());
    rep_sock.setsockopt (ZMQ_LINGER, &linger, sizeof(linger));

    zmq::pollitem_t items [] = {
        { rep_sock, 0, ZMQ_POLLIN, 0 }
    };

    while(!s_interrupted) {
        try {
            zmq::poll(&items [0], 1, HEARTBEAT_INTERVAL * 1000);
        }
        catch (zmq::error_t e) {
            DBG("Main serve thread interrupted: returning\n");
            break;
        }
        if(items[0].revents & ZMQ_POLLIN) {
            DBG("Received message on the control channel...\n");
            string client_id;
            string message_str = s_recv_envelope(rep_sock, client_id);
            DBG("client_id: %s\n", client_id.c_str());
            DBG("message_str: %s\n", message_str.c_str());
            stringstream ss(message_str);
            picojson::value message_j;
            ss >> message_j;
            MessageType msg_type = (MessageType) (message_j.get("type").get<double>()); 
            string msg_data = message_j.get("data").get<string>();
            DBG("msg_type: %d, msg_data: %s\n", msg_type, msg_data.c_str());

            if(filters.find(client_id) == filters.end()) {
                gettimeofday(&(filters[client_id].last_contact_time), NULL);
                filters[client_id].filter_vec = vector<PacketHistoryFilter>();
            }

            if(msg_type == ECHO_REQUEST) {
                DBG("Received ECHO_REQUEST message\n");
                DBG("Sending ECHO_REPLY message\n");
                EchoReplyMessage echo_rep_msg(msg_data);
                s_send_envelope(rep_sock, client_id, echo_rep_msg.serialize());
            }
            else if (msg_type == ADD_FILTER_REQUEST) {
                /*
                DBG("Received ADD_FILTER_REQUEST message\n");
                PacketHistoryFilter phf(msg_data.c_str());
                DBG("Created a PHF object for \"%s\"\n", phf.str().c_str());
                (filters[client_id].filter_vec).push_back(phf);
                printf("Added filter: \"%s\", from client: %s\n", msg_data.c_str(), client_id.c_str());
                AddFilterReplyMessage af_rep_msg(msg_data, true);
                s_send_envelope(rep_sock, client_id, af_rep_msg.serialize());
                */
            }
            else if (msg_type == DELETE_FILTER_REQUEST) {
                DBG("Received DELETE_FILTER_REQUEST message\n");
                ph_period = atoi(msg_data.c_str());
                // cout << "New packet history period = " << ph_period << endl;
                /*vector<PacketHistoryFilter>::iterator it = find((filters[client_id]).filter_vec.begin(), (filters[client_id]).filter_vec.end(), msg_data.c_str());
                if(it != (filters[client_id]).filter_vec.end()) {
                    printf("Deleted filter: \"%s\", from client: %s\n", msg_data.c_str(), client_id.c_str());
                    (filters[client_id]).filter_vec.erase(it);
                    DeleteFilterReplyMessage df_rep_msg(msg_data, true);
                    s_send_envelope(rep_sock, client_id, df_rep_msg.serialize());
                }
                else {
                    ERR("Could not find filter: \"%s\", from client: %s\n", msg_data.c_str(), client_id.c_str());
                    DeleteFilterReplyMessage df_rep_msg(msg_data, false);
                    s_send_envelope(rep_sock, client_id, df_rep_msg.serialize());
                }*/
            }
            else if (msg_type == GET_FILTERS_REQUEST) {
                DBG("Received GET_FILTERS_REQUEST message\n");
                vector<string> fvec;
                vector<PacketHistoryFilter> &v = (filters[client_id]).filter_vec;
                for(int i = 0; i < v.size(); i++) {
                    fvec.push_back((v[i]).str());
                }

                GetFiltersReplyMessage gf_rep_msg(fvec);
                s_send_envelope(rep_sock, client_id, gf_rep_msg.serialize());
            }
            else if (msg_type == CHANGE_PATH_REQUEST) {
                DBG("Received CHANGE_PATH_REQUEST message\n");
                EquivalenceClass ec = EquivalenceClass(msg_data);
                // FIXME: prototype. not very efficient or even readable. Should use JSON for everything.
                int offset = ec.toString().size();
                const char* extraInfo = msg_data.c_str() + offset + 1; // skips the ',' character
                string graphStr = string(extraInfo);

                // creating new graph as multimap
                std::multimap<string, string> graphAsMap;
                string currLine = graphStr;
                cout << graphStr;
                while (graphStr.length() > 1) { // if == 1, its only a '\n'
                    size_t pos = graphStr.find("\n");
                    currLine = graphStr.substr(0, pos);
                    graphStr = graphStr.substr(pos+1);
                    // TODO: or... use JSON :)
                    int p = 2, a = 1;
                    if(currLine[1] != ':') {
                        p = 3; a = 2;
                    }
                    for(int i = 2 ; i <= currLine.length() ; i++) {
                        if(i == currLine.length() || currLine[i] == ',' || currLine[i] == '\n') {
                            graphAsMap.insert(std::pair<string,string>(currLine.substr(0,a), currLine.substr(p,i-p)));
                            p = i+1;
                        }
                    }
                }
                pathsFromVF[ec] = graphAsMap;
            }
            else {
                ERR("Unexpected message type: %d\n", msg_type);
            }
        }

        /* Handle timeouts */
    }
    return;
}

void *
NetSight::postcard_worker(void *args)
{

    NetSight &n = NetSight::get_instance();
    return n.run_postcard_worker(args);
}

void*
NetSight::run_postcard_worker(void *args)
{
    sniff_pkts(sniff_dev.c_str());
}

void *
NetSight::history_worker(void *args)
{
    NetSight &n = NetSight::get_instance();
    return n.run_history_worker(args);
}

int histories, matches, okhist, nokhist, hops, notip, normalpackets = 0;

void*
NetSight::run_history_worker(void *args)
{
    zmq::socket_t pub_sock(context, ZMQ_PUB);
    stringstream pub_ss;
    int linger = 0;
    pub_ss << "tcp://*:" << NETSIGHT_HISTORY_PORT;
    pub_sock.bind(pub_ss.str().c_str());
    pub_sock.setsockopt (ZMQ_LINGER, &linger, sizeof(linger));

    struct timeval start_t, end_t;
    while(true) {
		int num_postcards, n_pc_ip, npc_not_ip;
        /*if(ph_period != PACKET_HISTORY_PERIOD) { // temporary hack to stop processing for some time
            usleep((long)(ROUND_LENGTH) * 1000);
            continue;
        }*/
        histories = matches = okhist = nokhist = hops = notip = 0;
        pthread_testcancel();
        DBG("----------------- ROUND COMPLETE ------------------\n\n");
        gettimeofday(&start_t, NULL);

        pthread_mutex_lock(&stage_lock);

        // Empty the contents of stage into a local PostcardList
        PostcardList pl(stage);
        stage.head = stage.tail = NULL;
        stage.length = 0;

        pthread_mutex_unlock(&stage_lock);

        // Empty the local PostcardList and populate path_table
        PostcardNode *pn = pl.head;
        num_postcards = pl.length;
		npc_not_ip = n_pc_ip = 0;
        DBG("Going to empty local PostcardList with %d postcards\n", num_postcards);
        for(int i = 0; i < num_postcards; i++) {
            DBG("pl.remove()\n");
            PostcardNode *next_pn = pn->next;
            pl.remove(pn);
			if(pn->pkt->eth.proto == 2048 && pn->pkt->eth.proto!=0x86dd) { // IPv4 proto				
				path_table.insert_postcard(pn);
			} else {
				//notip++;
				delete pn->pkt;
				delete pn;
				npc_not_ip++;
			}
            
            pn = next_pn;
        }
		n_pc_ip = num_postcards - npc_not_ip;

        // For each PostcardList older than ph_period:
        // - topo-sort the PostcardList
        // - match against all the filters
        unordered_map<std::string, PostcardList>::iterator it;
        for(it = path_table.table.begin(); it != path_table.table.end(); ) {
			cout << "Packet: " << it->second.tail->pkt->str_hex() << endl;
            PostcardNode *pn = it->second.tail;
            struct timeval curr;
            gettimeofday(&curr, NULL);
            double ts_diff = diff_time_ms(curr, pn->pkt->ts);
            if(ts_diff > ph_period) {
                PostcardList &pl = it->second;
                topo_sort(&pl, topo);
				histories++;
				perform_check(pl);
                pl.clear();
                it = path_table.table.erase(it);
            }
            else {
                it++;
            }
        }
        gettimeofday(&end_t, NULL);
        double diff_t = diff_time_ms(end_t, start_t);
        if(histories > 0) {
            cout << "pc:" 	<< num_postcards << "\t" 
				 << "t:" 	<< diff_t 		 << "\t" 
                 << "h:" 	<< histories 	 << "\t" 
				 << "ip4:" 	<< n_pc_ip 		 << "\t" 
				 << "!ip4:" << npc_not_ip 	 << "\t" 
				 << "hok:" 	<< okhist 		 << "\t" 
				 << "h!ok:" << nokhist 		 << "\t" 
				 << "sw:"<< hops 			 << "\t" 
				 << endl << flush;
        }
        normalpackets = 0;
        if(diff_t < ROUND_LENGTH) {
            usleep((long)(ROUND_LENGTH - diff_t) * 1000);
        }
    }
}

void printGraph(multimap<string, string> g) {
    multimap<string, string>::const_iterator itr;
    for(itr = g.begin(); itr != g.end(); itr++) {
        // TODO: use ranges to print all values of a key in the same line ?
        cout << itr->first << ":" << itr->second << endl;
    }
}

void printAllGraphs(std::vector<const std::multimap<std::string, std::string>*> vGraphs) {
    cout << "printing current graphs:" << endl;
    std::vector<const std::multimap<std::string, std::string>*>::iterator it;
    for (it = vGraphs.begin() ; it != vGraphs.end() ; it++) {
        cout << "<<<<Graph>>>>" << endl;
        printGraph(**it);
    }
}

string NetSight::getExpectedPath(PostcardList pl) {
    return string("??? No EC found!");
}

/**
  * Gets expected graph of the postcard
  */
const multimap<string, string> *getExpectedGraph(PostcardNode *p) {
    Packet *pkt = p->pkt;

    unordered_map<EquivalenceClass, multimap<string, string>>::const_iterator itr;
    for(itr = pathsFromVF.begin(); itr != pathsFromVF.end(); itr++) {
        EquivalenceClass ec = itr->first;
        if(ec.lowerBound[NW_SRC] > pkt->nw_src() || pkt->nw_src() > ec.upperBound[NW_SRC]) {
            continue;
        } else if (ec.lowerBound[NW_DST] > pkt->nw_dst() || pkt->nw_dst() > ec.upperBound[NW_DST]) {
            continue;
        } else if (ec.lowerBound[TP_SRC] > pkt->tp_src() || pkt->tp_src() > ec.upperBound[TP_SRC]) {
            continue;
        } else if (ec.lowerBound[TP_DST] > pkt->tp_dst() || pkt->tp_dst() > ec.upperBound[TP_DST]) {
            continue;
        } else if (ec.lowerBound[NW_PROTO] > pkt->nw_proto() || pkt->nw_proto() > ec.upperBound[NW_PROTO]) {
            continue;
        }
        matches++;
        return &itr->second;
    }
    // TODO: no EC found
    cout << "No EC found.........." << endl << flush;
    return NULL;
}

void NetSight::sniff_pkts(const char *dev) {
    char errbuf[PCAP_ERRBUF_SIZE];          /* error buffer */ 
    const char *filter_exp = POSTCARD_FILTER;/* filter expression [3] */
    bpf_u_int32 mask;                       /* subnet mask */
    bpf_u_int32 net;                        /* ip */
    int num_packets = -1;                   /* number of packets to capture */

    packet_init();

    /* get network number and mask associated with capture device */
    if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
        ERR("Couldn't get netmask for device %s: %s\n",
                dev, errbuf);
        net = 0;
        mask = 0;
    }

    /* print capture info */
    printf("Device: %s\n", dev);
    printf("Number of packets: %d\n", num_packets);
    printf("Filter expression: %s\n", filter_exp);

    /* open capture device */
    postcard_handle = pcap_open_live(dev, SNAP_LEN, 1, 100, errbuf);
    if (postcard_handle == NULL) {
        ERR("Couldn't open device %s: %s\n", dev, errbuf);
        return;
    }

    /* make sure we're capturing on an Ethernet device [2] */
    if (pcap_datalink(postcard_handle) != DLT_EN10MB) {
        ERR("%s is not an Ethernet\n", dev);
        return;
    }

    /* compile the filter expression */
    if (pcap_compile(postcard_handle, &postcard_fp, filter_exp, 0, net) == -1) {
        ERR("Couldn't parse filter %s: %s\n",
                filter_exp, pcap_geterr(postcard_handle));
        return;
    }

    /* apply the compiled filter */
    if (pcap_setfilter(postcard_handle, &postcard_fp) == -1) {
        ERR("Couldn't install filter %s: %s\n",
                filter_exp, pcap_geterr(postcard_handle));
        return;
    }

    struct pcap_pkthdr hdr;
    while(true) {
        pthread_testcancel();
        const u_char *pkt = pcap_next(postcard_handle, &hdr);
        if(pkt == NULL)
            continue;
        DBG("Got postcard: %p\n", pkt);
        postcard_handler(&hdr, pkt);
    }
    DBG("Error! Should never come here...\n");

}

void
NetSight::cleanup()
{
    /* cleanup */
    void* ret = NULL;
    pthread_cancel(postcard_t);
    pthread_cancel(history_t);
    pthread_join(postcard_t, &ret);
    DBG("Postcard thread joined\n");
    pthread_join(history_t, &ret);
    DBG("History thread joined\n");

    pcap_freecode(&postcard_fp);
    if(postcard_handle) {
        pcap_close(postcard_handle);
    }

    DBG("\nCleanup complete.\n");
}

/*
 * Add received postcard to stage
 * Use stage_lock to write in a thread-safe manner
 * */
void
NetSight::postcard_handler(const struct pcap_pkthdr *header, const u_char *packet)
{
    static u32 packet_number = 0;
    pthread_mutex_lock(&stage_lock);
    Packet *p = new Packet(packet, header->len, 
                    SKIP_ETHERNET, packet_number++, header->caplen);
    if(p->eth.vlan != DEBUG_VLAN) {
        // Normal packet, not a postcard
        normalpackets++;
        delete(p);
        pthread_mutex_unlock(&stage_lock);
        return;
    }
    PostcardNode *pn = new PostcardNode(p);
    assert(psid_to_dpid.find(pn->dpid) != psid_to_dpid.end());
    pn->dpid = psid_to_dpid[pn->dpid];
    stage.push_back(pn);
    DBG("Adding postcard to stage: length = %d\n", stage.length);
    // cout << "Inserting postcard " << pn << " " << pn->get_dpid() << " " << pn->get_outport() << endl;
    Packet *pkt = (stage.tail)->pkt;
    pkt->ts = header->ts;
    pthread_mutex_unlock(&stage_lock);
}

void
NetSight::sig_handler(int signum)
{
    NetSight &n = NetSight::get_instance();

    switch(signum) {
        case SIGINT:
            DBG("Got SIGINT\n");
            n.s_interrupted = true;
            n.cleanup();
            break;
        default:
            printf("Unknown signal %d\n", signum);
    }
}

void 
NetSight::connect_db(string host)
{
    if(!topo_db.connect(host)) {
        ERR(AT "Could not connect to MongoDB: %s\n", host.c_str());
        exit(EXIT_FAILURE);
    }
    if(!ft_db.connect(host)) {
        ERR(AT "Could not connect to MongoDB: %s\n", host.c_str());
        exit(EXIT_FAILURE);
    }
    if(!psid_db.connect(host)) {
        ERR(AT "Could not connect to MongoDB: %s\n", host.c_str());
        exit(EXIT_FAILURE);
    }

    read_topo_db();
    read_psid_db();
}

void 
NetSight::read_topo_db()
{
    DBG("Reading topology from the DB:\n");
    unique_ptr<mongo::DBClientCursor> cursor = topo_db.get_records(mongo::BSONObj());
    if(cursor->more()) {
        stringstream ss;
        mongo::BSONObj obj = cursor->next();
        DBG("%s\n", obj.jsonString().c_str());
        ss << obj.jsonString();
        topo.read_topo(ss);
    }
    else {
        DBG("No topology info in the DB\n");
    }
}

void 
NetSight::read_psid_db()
{
    DBG("Reading psid-to-dpid from the DB:\n");
    unique_ptr<mongo::DBClientCursor> cursor = psid_db.get_records(mongo::BSONObj());
    while(cursor->more()) {
        mongo::BSONObj obj = cursor->next();
        int psid = (int) obj["psid"].Int();
        int dpid = (int) obj["dpid"].Int();
        psid_to_dpid[psid] = dpid;
        DBG("%d -> %d\n", psid, dpid);
    }
}

void 
NetSight::get_flow_entry(int dpid, int version, string &match, vector<string> &actions)
{
    DBG("Reading flow table entry from the DB:\n");
    DBG("dpid:%d, version: %d\n", dpid, version);
    mongo::BSONObjBuilder query;
    query << "dpid" << dpid << "version" << version;
    unique_ptr<mongo::DBClientCursor> cursor = ft_db.get_records(query.obj());
    if(cursor->more()) {
        mongo::BSONObj obj = cursor->next();
        DBG("%s\n", obj.jsonString().c_str());
        match = obj["match"].toString();
        vector<mongo::BSONElement> action_obj = obj["action"].Array();
        for(int i = 0; i < action_obj.size(); i++) {
            actions.push_back(action_obj[i].toString());
        }
    }
    else {
        DBG("Error: No matching flow-entry found...\n");
    }
}

void rewindTwoSwitches(const char *&path, const char *pinit)
{
    for(int nSplits = 0; nSplits < 2; path--)
    {
        if(path == pinit)
        {
            path = path - 2; // will be increased again after the for
            break;
        }
        else if(*path == '\0' || *path == ',')
        {
            nSplits++;
        }
    }
    path = path + 2;
}

bool compare_paths(string realPath, string expectedPath)
{
    const char *p1, *p2, *s1 = realPath.c_str(), *s2 = expectedPath.c_str();
    for (p1 = s1, p2 = s2 ; *p1 && *p1 == *p2 ; p1++, p2++)
        ; // Finding byte of divergence or end of paths
    if(!*p1 && !*p2)
    {
        // print_color(ANSI_COLOR_BLUE, "OK\n");
        return true;
    }
    else
    {
        rewindTwoSwitches(p1, s1);
        rewindTwoSwitches(p2, s2);

        int e1, e2, a1, a2;
        int nReads;
        nReads = sscanf(p1, "%d,%d", &a1, &a2);
        if(nReads == 1)
        {
            a2 = -1;
        }
        nReads = sscanf(p2, "%d,%d", &e1, &e2);
        if(nReads == 1)
        {
            e2 = -1;
        }
        print_color(ANSI_COLOR_RED, "Error in switch %d: expected next hop %d, found %d\n", a1, e2, a2);
        cout << "Real:     " << realPath << endl;
        cout << "Expected: " << expectedPath << endl;
        return false;
    }
}

void get_next_expected_switches(string dpid,
                        std::vector<const std::multimap<std::string, std::string>*> &vGraphs,
                        std::list<string> &switchList)
{
    std::vector<const std::multimap<std::string, std::string>*>::iterator itr;
    for (itr = vGraphs.begin() ; itr != vGraphs.end() ; itr++) {
        std::pair <std::multimap<std::string,std::string>::const_iterator,
                   std::multimap<std::string,std::string>::const_iterator> itrs;
        itrs = (*itr)->equal_range(dpid);
        for (std::multimap<std::string,std::string>::const_iterator it=itrs.first; it!=(*itr)->end() && it!=itrs.second; ++it) {
            if(it->first == dpid && it->second != "H") {
                //bool alreadyIn = false;
                //for (std::list<string>::iterator it3 = switchList.begin() ; it3 != switchList.end() ; it3++) {
                //    if(*it3 == it->second) {
                //        alreadyIn = true;
                //    }
                //}
                //if (!alreadyIn) {
                    switchList.push_back(it->second);
                //}
            }
        }
    }
}


bool check_link(string dpid, std::list<string> &switchQueue) {
    std::list<std::string>::iterator it;
    for (it = switchQueue.begin() ; it != switchQueue.end() ; it++) {
        if(*it == dpid) {
            return true;
        }
    }
    return false;
}

bool has_links(string dpid, std::vector<const std::multimap<std::string, std::string>*> &vGraphs)
{
    std::vector<const std::multimap<std::string, std::string>*>::iterator itr;
    for (itr = vGraphs.begin() ; itr != vGraphs.end() ; itr++) {
        std::pair <std::multimap<std::string,std::string>::const_iterator,
                   std::multimap<std::string,std::string>::const_iterator> itrs;
        itrs = (*itr)->equal_range(dpid);
        for (std::multimap<std::string,std::string>::const_iterator it=itrs.first; it!=(*itr)->end() && it!=itrs.second; ++it) {
            if(!it->second.empty() && it->second.find_first_not_of("0123456789") == std::string::npos) {
                return true;
            }
        }
    }
    return false;
}

bool count_links(string dpid, std::vector<const std::multimap<std::string, std::string>*> &vGraphs)
{
    int res = 0;
    std::vector<const std::multimap<std::string, std::string>*>::iterator itr;
    for (itr = vGraphs.begin() ; itr != vGraphs.end() ; itr++) {
        std::pair <std::multimap<std::string,std::string>::const_iterator,
                   std::multimap<std::string,std::string>::const_iterator> itrs;
        itrs = (*itr)->equal_range(dpid);
        for (std::multimap<std::string,std::string>::const_iterator it=itrs.first; it!=(*itr)->end() && it!=itrs.second; ++it) {
            if(!it->second.empty() && it->second.find_first_not_of("0123456789") == std::string::npos) {
                res++;
            }
        }
    }
    return res;
}

void merge_graphs(std::vector<const std::multimap<std::string, std::string>*> &vGraphs, PostcardList packet_history) {
    for (PostcardNode *pn = packet_history.head ; pn != NULL ; pn = pn->next) {
        const multimap<string, string> *graph = getExpectedGraph(pn);
        if(graph == NULL) return;
        // detect header modifications
        bool found = false;
        std::vector<const std::multimap<std::string, std::string>*>::iterator it;
        for (it = vGraphs.begin() ; it != vGraphs.end() ; it++) {
            if (*it == graph) {
                found = true;
                break;
            }
        }
        // merge graphs if header was modified:
        // this does not mean that they are connected
        // we will check this as we search in graphs
        // also, we shouldn't need to merge if netsight was saving the modifications
        // or at least sending postcards before the modifications rather than
        // after them.
        const bool foundMod = !found;
        if(foundMod) {
            vGraphs.push_back(graph);
        }
    }
}

/**
  * -- JMir --
  * NOTE 1:
  * implementing comparison between mods would required more time
  * chaning VF and NS but we can just check if there are any mods
  * (by checking if the EC is the same as the already seen in this
  * packet history)
  * NOTE 2: (TODO: Review this comment after algorithm is completed)
  * Since netsight "instruments" switches to send postcards
  * after the packet headers are modified (when they are), we must use
  * the previous postcard instead of the current one to get the expected graph.
  * We must therefore assume that the first switch does not modify headers.
  * Also, we assume that the first switch is in the expected path. Otherwise,
  * it would be a problem in the sender host rather than in the switches.
  */
void perform_check(PostcardList& packet_history) {
    std::vector<const std::multimap<std::string, std::string>*> vGraphs;
    std::list<string> switchQueue;

    cout << "<Packet history: ";
    for(PostcardNode *pn = packet_history.head ; pn != NULL ; pn = pn->next) {
        cout << pn->dpid << ",";
    }
    cout << ">" << endl << endl << flush;

    merge_graphs(vGraphs, packet_history);

    // See NOTE 2 above function definition
    string prev = to_string(packet_history.head->dpid);
    packet_history.erase(packet_history.head);
    switchQueue.push_back(prev);

    int expectedBranches = -1;

    // printAllGraphs(vGraphs);

    while (!switchQueue.empty()) {
        prev = switchQueue.front();
        switchQueue.pop_front();
        std::list<string> expectedSwitches;
        get_next_expected_switches(prev, vGraphs, expectedSwitches);
        int found = 0, to_find = expectedSwitches.size();
        std::list<string>::iterator it;
        cout << "Expected Switches after " << prev << " : ";
        for (it = expectedSwitches.begin() ; it != expectedSwitches.end() ; it++) {
            switchQueue.push_back(*it);
            cout << " " << *it;
        }
        cout << endl;
        for (it = expectedSwitches.begin() ; it != expectedSwitches.end() ; it++) {
            // cout << "Checking s" << *it;  
            for (PostcardNode *pn = packet_history.head ; found < to_find && pn != NULL ; ) {
                hops++;
                // cout << " against s" << pn->dpid << endl << flush;
                if(*it == to_string(pn->dpid)) {
                    // cout << "FOUND!" << endl << flush;
                    PostcardNode *t = pn;
                    pn = pn->next;
                    packet_history.erase(t);
                    found++; // TODO: store which were found to present in case of bug
                    break;
                }
                pn = pn->next;
            }
        }
        // cout << endl;
        expectedBranches = expectedSwitches.size();
        if(found != expectedSwitches.size()) {
            nokhist++;
            //print_color(ANSI_COLOR_RED, "Error: PL_LEN = %d | EXP_SW = %d\n", packet_history.length, expectedSwitches.size());
            if (expectedSwitches.size() == 1) {
                //print_color(ANSI_COLOR_RED, "Error in s%s: ERR_DROPPING_BUT_SHOULD_FORWARD\n", prev.c_str());
                // TODO: point out expected
            } else {
                //print_color(ANSI_COLOR_RED, "Error in s%s: ERR_BRANCH_NOT_FOLLOWED\n", prev.c_str());
                // TODO: point out expected
            }
            return;
        }

        //cout << "[Q] ";
        //for (std::list<string>::iterator it = switchQueue.begin() ; it != switchQueue.end() ; it++) {
        //    cout << "," << *it << flush;
        //}
        //cout << endl << "Popping!" << endl << flush;
    }
    
    if(packet_history.length == 0) {
        okhist++;
        // print_color(ANSI_COLOR_BLUE, "OK\n");
    } else {
        nokhist++;
        print_color(ANSI_COLOR_RED, "Graph ended with postcards left to analyse\n");
        /*
        std::list<string> expectedSwitches;
        // HACK: Should detect the previous switch using input field
        // or searching the output ports of the previously analysed postcards
        if(packet_history.head->dpid == 6)
            prev = "5";
        else if (packet_history.head->dpid == 7) {
            prev = "2";
        } else {
            std::cout << "This should not happen in this topology!!";
        }
        get_next_expected_switches(prev, vGraphs, expectedSwitches);
        if(expectedSwitches.size() == 0) {
            print_color(ANSI_COLOR_RED, "Error in s%s: ERR_FORWARDING_BUT_SHOULD_DROP\n", prev.c_str());
        } else {
            print_color(ANSI_COLOR_RED, "Error in s%s: ERR_UNEXPECTED_BRANCH\n", prev.c_str());
        }
        */
    }
}