/*
 * Switch.cc
 *
 *  Created on: Jul 22, 2016
 *      Author: miranda
 */

#include "Switch.hh"
#include <iostream>

const std::list<const char*>& Switch::getTable(uint64_t version) const {
	if(!tables.empty()) {
		return tables.back().second;
	}
}

void Switch::addFlow(const char* flow) {
	// FIXME: prototype. Should know where to put the flow.
	std::list<const char*> newTable;
	if(!tables.empty()) {
		newTable.assign(tables.back().second.begin(),tables.back().second.end());
	}
	newTable.push_back(flow); // TODO: should probably copy this string...
	tables.push_back(std::pair<uint64_t, std::list<const char*>>(tables.back().first + 1 , newTable));
}

void Switch::modFlows(const char* flow) {
	// TODO
}

void Switch::delFlows(const char* match) {
	// TODO
}
