/*
 * Copyright 2014, Stanford University. This file is licensed under Apache 2.0,
 * as described in included LICENSE.txt.
 *
 * Author: nikhilh@cs.stanford.com (Nikhil Handigol)
 *         jvimal@stanford.edu (Vimal Jeyakumar)
 *         brandonh@cs.stanford.edu (Brandon Heller)
 */

#include <getopt.h>
#include <vector>
#include <string>
#include <pthread.h>

#include "api.hh"
#include "ndb.hh"
#include "types.hh"
#include "helper.hh"
#include "postcard.hh"
#include "packet.hh"

int 
main(int argc, char **argv)
{
    NDB &ndb = NDB::get_instance();
    ndb.start();
    return 0;
}

