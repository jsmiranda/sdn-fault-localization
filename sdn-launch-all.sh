# Script version 0.1 - values are set as constants instead of inputs
# and changing topologies and controller also implies changing the script

# This script assumes installation of all software - instructions to be
# added later...

readonly TRUE=1
readonly FALSE=0

readonly ROOT_DIR=$PWD

readonly DEFAULT_NETSIGHT_LISTEN_PORT=10000
readonly DEFAULT_CONTROLLER_PORT=6633

# The variables below should be given as inputs
# which proxies are on:
readonly VF_ON=TRUE
readonly NS_ON=TRUE
# topology details
readonly NETSIGHT_INTERFACE="s1-eth1"
readonly TOPO_NUMBER="linear-10-2"


# VeriFlow and Netsight are ON
if [ $VF_ON == TRUE -a $NS_ON == TRUE ]; then
	mn_controller_port=$DEFAULT_CONTROLLER_PORT # MiniNet connects to NetSight (at default port)
	veriflow_port=$DEFAULT_NETSIGHT_LISTEN_PORT # NetSight listens to VeriFlow
	controller_port=5555 # set controller port to a different value
# NetSight ON (only)
elif [ $NS_ON == TRUE ]; then
	mn_controller_port=$DEFAULT_CONTROLLER_PORT   # MiniNet connects to NetSight (at default port)
	controller_port=$DEFAULT_NETSIGHT_LISTEN_PORT # set controller port to NetSight default listen port
# VeriFlow ON (only)
elif [ $VF_ON == TRUE ]; then
	veriflow_port=$DEFAULT_CONTROLLER_PORT # Set a port to VeriFlow
	mn_controller_port=$veriflow_port # connect MiniNet to this port
	controller_port=5555 # set controller port to a different value
# No proxies ON
else
	# Set a port to the controller and connect MiniNet to this port
	controller_port=$DEFAULT_CONTROLLER_PORT
	mn_controller_port=$controller_port
fi


case $1 in
	1)
	echo Starting Controller &&
	cd ${ROOT_DIR}/pox/ && ./pox.py forwarding.l2_learning openflow.of_01 --port=${controller_port} #POX l2
	# cd ${ROOT_DIR}/pox/ && ./pox.py forwarding.l2_multi openflow.discovery openflow.of_01 --port=${controller_port} #POX l2 multi
	# cd ${ROOT_DIR}/pox/ && ./pox.py forwarding.l3_learning openflow.of_01 --port=${controller_port} #POX l3
	# cd ${ROOT_DIR}/pox/ && ./pox.py samples.spanning_tree openflow.of_01 --port=${controller_port}    #POX STP
	# cd ${ROOT_DIR}/nox/build/src/ && nox_core -i ptcp:${controller_port} switch                     #NOX
	;;
	2) echo Starting VeriFlow && rm -f forwardingGraphs.txt &&
	cd ${ROOT_DIR}/VeriFlow-v0.3/VeriFlow &&
	./VeriFlow ${veriflow_port} 127.0.0.1 ${controller_port} ${ROOT_DIR}/topologies/veriflow/${TOPO_NUMBER}.txt
	;;
	3) echo Starting MiniNet &&
	cd ${ROOT_DIR} &&
	sudo  mn --topo linear,10,2 --mac --switch ovsk --controller=remote,ip=127.0.0.1,port=${mn_controller_port}
	#sudo mn --topo customtopo --custom ${ROOT_DIR}/topologies/mininet/${TOPO_NUMBER}.py \
	#--mac --switch ovsk --controller=remote,ip=127.0.0.1,port=${mn_controller_port}
	;;
	4) echo Starting NetSight Flow Table State Recorder &&
	cd ${ROOT_DIR}/NetSight/recorder &&
	python ndb.py --topo ${ROOT_DIR}/topologies/netsight/${TOPO_NUMBER} --in-band
	;;
	5) echo Starting NetSight server && rm -f packetHistories.txt &&
	cd ${ROOT_DIR}/NetSight/netsight/src/ &&
	sudo -S LD_LIBRARY_PATH=/home/mininet/NetSight/netsight/src/filter/bpf-linux/ \
	./netsight --i ${NETSIGHT_INTERFACE} --t ${ROOT_DIR}/topologies/netsight/${TOPO_NUMBER}
	# gdb --args ./netsight --i ${NETSIGHT_INTERFACE} --t ${ROOT_DIR}/topologies/netsight/${TOPO_NUMBER}
	# valgrind --leak-check=yes ./netsight --i ${NETSIGHT_INTERFACE} --t ${ROOT_DIR}/topologies/netsight/${TOPO_NUMBER}
	;;
	*) echo "Please use a valid number."
esac
