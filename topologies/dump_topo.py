#!/usr/bin/python
import sys
import json
import re
import importlib
from mininet.topo import Topo
from mininet.topo import LinearTopo
from mininet.node import Node
from mininet_topos import *

# borrowed from Mininet code
def get_dpid(topo, sw_name):
    try:
        dpid = sw_name[1:]
        return int(dpid)
    except IndexError:
        raise Exception( 'Unable to derive default datapath ID - '
                         'please either specify a dpid or use a '
                         'canonical switch name such as s23.' )

def dump_out_band_topo(topo, outfile=None, collector_sw=['s0',]):
    '''Dump out-of-band topology to be used as ndb input
    Assumption: s0 is the collector switch 
    to which all switches are directly connected'''

    f = open(outfile, 'w') if outfile else sys.stdout

    # output json object
    topo_j = {'topo': {'nodes': [], 
                        'links': []
                    },
            'collector': []
            }

    # list all switches except s0
    node_to_dpid = {}
    switches = list(set(topo.switches()).difference(set(collector_sw))) 
    for s in switches:
        node_to_dpid[s] = get_dpid(topo, s)

    # list links
    links = []
    for (u,v) in topo.links():
        # consider only sw-sw links
        if u in topo.hosts() or v in topo.hosts():
            continue
        if u in collector_sw or v in collector_sw:
            continue

        dpid1 = node_to_dpid[u]
        dpid2 = node_to_dpid[v]
        port1, port2 = topo.port(u, v)
        links.append({'src_dpid':dpid1, 'src_port':port1, 'dst_dpid':dpid2, 'dst_port':port2})

    topo_j['topo']['nodes'] = [{"name":k, "dpid":v} for (k, v) in node_to_dpid.iteritems()]
    topo_j['topo']['links'] = links

    # list collector location
    collector_loc = []
    for s in switches:
        s_coll = False
        for cs in collector_sw:
            if cs in topo.g[s]:
                dpid = node_to_dpid[s]
                port = topo.port(s, cs)[0]
                collector_loc.append({'dpid':dpid, 'port':port})
                s_coll = True
                break

        if not s_coll:
            print 'Warning: Switch %s does not have a link to the collector switch' % s
    topo_j['collector'] = collector_loc

    print >>f, json.dumps(topo_j)
    if outfile:
        f.close()

    return topo_j


def dump_in_band_topo(topo, outfile=None):
    '''Dump in-band topology to be used as ndb input'''
    f = open(outfile, 'w') if outfile else sys.stdout

    # output json object
    topo_j = {'topo': {'nodes': [], 
                        'links': []
                    },
            'collector': []
            }

    # list all switches
    node_to_dpid = {}
    switches = topo.switches()
    for s in switches:
        node_to_dpid[s] = get_dpid(topo, s)

    # list links
    links = []
    for (u,v) in topo.links():
        # consider only sw-sw links
        if u in topo.hosts() or v in topo.hosts():
            continue

        dpid1 = node_to_dpid[u]
        dpid2 = node_to_dpid[v]
        port1, port2 = topo.port(u, v)
        links.append({'src_dpid':dpid1, 'src_port':port1, 'dst_dpid':dpid2, 'dst_port':port2})

    topo_j['topo']['nodes'] = [{"name":k, "dpid":v} for (k, v) in node_to_dpid.iteritems()]
    topo_j['topo']['links'] = links

    # collector location
    collector_sw = topo.g[collector_host].keys()[0]
    collector_dpid = node_to_dpid[collector_sw]
    collector_port, _ = topo.port(collector_sw, collector_host)
    topo_j['collector'] = {'dpid':collector_dpid, 'port':collector_port}

    print >>f, json.dumps(topo_j)
    if outfile:
        f.close()

    return topo_j

def dump_in_band_topo_txt(topo, outfile=None):
    '''Dump in-band topology to be used as VeriFlow input'''

    f = open(outfile, 'w') if outfile else sys.stdout

    # output string
    topo_j = ""

    # list all switches
    node_to_dpid = {}
    switches = topo.switches()
    hosts = topo.hosts()
    switches_to_portAndNextHop = {}
    i = 0
    for s in switches:
        switches_to_portAndNextHop[s] = [] # empty list of links
        node_to_dpid[s] = get_dpid(topo, s)
    for h in hosts:
        switches_to_portAndNextHop[h] = [] # non initialized switch IP


    # list links
    for (u,v) in topo.links():
        switches_to_portAndNextHop[u].append((topo.port(u, v)[0], v))
        switches_to_portAndNextHop[v].append((topo.port(v, u)[0], u))

    i = 1
    for s in switches:
        topo_j += str(i) + ' ' + node_to_IP(topo, s) + ' 0 ' + links_to_str(topo, switches_to_portAndNextHop[s]) + '\n'
        i += 1

    print >>f, topo_j
    if outfile:
        f.close()

    return topo_j

def node_to_IP(topo, node_name):
    host_offset = 16777216 * 10
    switch_offset = host_offset + 65536 * 1

    if node_name in topo.switches():
        node_number = switch_offset + topo.switches().index(node_name) + 1
    elif node_name in topo.hosts():
        node_number = host_offset + topo.hosts().index(node_name) + 1

    return Int2IP(node_number)

def links_to_str(topo, lst):
    sup_lim = len(lst) + 1

    res = ''
    for i in range(1, sup_lim):
        for link in lst:
            if link[0] == i:
                res += str(link[0]) + ' ' + node_to_IP(topo, link[1]) + ' '
    return res

def Int2IP(ipnum):
    o1 = int(ipnum / 16777216) % 256
    o2 = int(ipnum / 65536) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()



collector_host = 'h1'
collector_sw = 's7'

TOPO = 'ft4' 
topo_x = ft4.FatTreeTopo()
dump_in_band_topo(topo_x, 'netsight/' + TOPO)
dump_in_band_topo_txt(topo_x, 'veriflow/' + TOPO + '.txt')
