#!/usr/bin/python

from mininet.topo import Topo
from mininet.node import Node
from mininet.topo import LinearTopo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.node import Host
from mininet.link import TCLink
from mininet.link import Link
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.cli import CLI

class FatTreeTopo(Topo):
    def build(self, k=4):
        n_core_switches = k*k/4
        n_pods = k
        n_pod_switches = k
        current_switch = 1
        current_host = 1

        core_switches = []
        for i in range(n_core_switches):
            core_switches.append('s%i' % (i + current_switch))
            self.addSwitch(core_switches[i])
        current_switch += n_core_switches

        # for each pod
        for a in range(n_pods):
            # create aggr switches and links to core sws
            aggr_switches = []
            for b in range(k / 2):
                aggr_switches.append('s%i' % (b + current_switch))
                self.addSwitch(aggr_switches[b])
                # link to k / 2 core switches
                for c in range (k / 2):
                    self.addLink(aggr_switches[b], core_switches[b * k / 2 + c])
            current_switch += k / 2
            # create edge switches and links to aggr sws + hosts and links to edges
            edge_switches = []
            for b in range(k / 2):
                edge_switches.append('s%i' % (b + current_switch))
                self.addSwitch(edge_switches[b])
                # link to all aggr switches
                for c in range (k / 2):
                    self.addLink(edge_switches[b] , aggr_switches[c])
                # create hosts and respective links
                for c in range (k / 2):
                    self.addHost('h%i' % (current_host + c))
                    self.addLink(edge_switches[b] , 'h%i' % (current_host + c))
                current_host += k / 2
            current_switch += k / 2

topos = { 'FatTreeTopo': ( lambda: FatTreeTopo(8) ) }
