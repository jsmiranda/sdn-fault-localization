from mininet.topo import Topo
from mininet.node import Node

class CustomTopo( Topo ):
        "Simple topology"
        
        def __init__(self, enable_all = True):
                "Create custom topo"
                
                super( CustomTopo, self).__init__()
                               
                #Add Switches
                self.addSwitch("s1")
                self.addSwitch("s2")

                #Add Nodes
                self.addHost("h1") # Logger
                self.addHost("h2")
                self.addHost("h3")
                self.addHost("h4")

                #Add Links
                self.addLink( "h1", "s2" )  # Logger

                self.addLink( "s1", "s2" )
                self.addLink( "h2", "s1" )
                self.addLink( "h3", "s2" )
                self.addLink( "h4", "s2" )

topos = { 'customtopo': ( lambda: CustomTopo() ) }
