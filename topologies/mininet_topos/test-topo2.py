from mininet.topo import Topo
from mininet.node import Node

class CustomTopo( Topo ):
        "Simple topology"
        
        def __init__(self, enable_all = True):
                "Create custom topo"
                
                super( CustomTopo, self).__init__()
                               
                #Add Switches
                self.addSwitch("s1")
                self.addSwitch("s2")
                self.addSwitch("s3")

                #Add Nodes
                self.addHost("h1")
                self.addHost("h2")
                self.addHost("h3")
                self.addHost("h4")
                self.addHost("ns")

                #Add Links - collector <-> normal switches
                self.addLink( "s1", "s2" )
                self.addLink( "s1", "s3" )

                #Links among switches
                self.addLink( "s2", "s3" )

                #Host-Switch links
                self.addLink( "h1", "s2" )
                self.addLink( "h2", "s2" )
                self.addLink( "h3", "s3" )
                self.addLink( "h4", "s3" )

		# collector <-> netsight logger links
                self.addLink( "ns", "s1" )

topos = { 'customtopo': ( lambda: CustomTopo() ) }
