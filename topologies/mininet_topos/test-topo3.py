from mininet.topo import Topo
from mininet.node import Node

class CustomTopo( Topo ):
        "Simple topology"
        
        def __init__(self, enable_all = True):
                "Create custom topo"
                
                super( CustomTopo, self).__init__()
                               
                #Add Switches
                self.addSwitch("s1")
                self.addSwitch("s2")
                self.addSwitch("s3")
                self.addSwitch("s4")
                self.addSwitch("s5")
                self.addSwitch("s6")
                self.addSwitch("s7")

                #Add Nodes
                self.addHost("h1") # Logger
                self.addHost("h2")
                self.addHost("h3")
                self.addHost("h4")

                #Add Links
                self.addLink( "h1", "s1" ) # Logger
                self.addLink( "h2", "s2" ) # NetSheriff cant detect drop in the edge
                self.addLink( "h3", "s3" )
                self.addLink( "h4", "s6" )

                self.addLink( "s1", "s2" )
                self.addLink( "s2", "s3" )
                self.addLink( "s2", "s4" )
                self.addLink( "s2", "s7" )
                self.addLink( "s4", "s5" )
                self.addLink( "s5", "s6" )

topos = { 'customtopo': ( lambda: CustomTopo() ) }
